#include "EuclidAlgo.h"

EuclidAlgo::EuclidAlgo(int new_m, int new_n)
	: m		  (new_m)
	, n		  (new_n)
	, a		  (0)
	, b		  (1)
	, c		  (new_m)
	, d		  (new_n)
	, a_dash  (1)
	, b_dash  (0)
	, quotient(0)
	, rest	  (0)
	, result  (-1)
{ 
	//algoE(m, n);
	algoE(c, d);
}

int EuclidAlgo::getResult()
{
	return result;
}

void EuclidAlgo::getDivisionRest(int c, int d)
{
	try {
		//std::cout << "c: " << c << std::endl << "d: " << d << std::endl;
		int div_rest = c % d;
		int div_quotient = c / d;

		if(div_rest >= 0 && div_rest < d) {
			rest = div_rest;
			quotient = div_quotient;
			//std::cout << "Rest: " << rest << std::endl;
		}
		else {
			//wrong rest
			throw div_rest;
		}
	}
	catch (int i) {
		std::cout << "Wrong rest: " << i << std::endl;
	}
}

int EuclidAlgo::algoE(int c, int d)
{
	while (rest >= 0 && rest <= d) {
		//step E2
		getDivisionRest(c, d);
		
		//step E3
		if (rest == 0) {
			result = a * m + b * n;
			break;
		}

		//step E4
		int temp = a_dash;
		c		 = d;
		d		 = rest;
		a_dash   = a;
		a		 = temp - quotient * a;
		temp	 = b_dash;
		b_dash	 = b;
		b		 = temp - quotient * b;
	}


	std::cout << "Result: " << result << std::endl;
	return result;
}

