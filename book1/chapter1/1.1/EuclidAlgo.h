#pragma once

#include <iostream>
#include <exception>

class EuclidAlgo {
public:
	EuclidAlgo(int new_m, int new_n);

	int getResult();
private:
	void getDivisionRest(int c, int d);
	
	int algoE(int c, int d);

	int m;
	int n;

	int a;
	int b;
	int a_dash;
	int b_dash;

	int c;
	int d;

	int quotient;
	int rest;

	int result;
};

