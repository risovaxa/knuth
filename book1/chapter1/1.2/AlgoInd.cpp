#include "AlgoInd.h"

AlgoInd::AlgoInd(int new_n)
	: n(new_n) 
	, result(0)
{
	int k = 1;

	//step 2
	while (n != k) {
		if(result = statement(k)) { 
			//std::cout << "Method result == " << result << " on step: " << k << std::endl;
			++k;
		}
		else {
			break;
		}
	}
	std::cout << "Final result: " << result << std::endl;
}

bool AlgoInd::statement(int n)
{
	int k;
	int sum = 0;
	int square = 0;
	for (k = 1; k <= n; k++) {
		sum += (2 * k - 1);
		square = k * k;
		//std::cout << "\t Sum == " << sum << std::endl;
		//std::cout << "\t Square == " << square << std::endl;
	}
	if (sum == square) 
		return true;
	else 
		return false;
}
