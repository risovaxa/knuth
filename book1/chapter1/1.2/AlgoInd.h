#pragma once

#include <iostream>

class AlgoInd
{
public:
	AlgoInd(int new_n);

private:
	bool result;
	bool statement(int n);
	int n;
};

